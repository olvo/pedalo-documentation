---
layout: page
title: Configuration client
category: Config
permalink: /client-configuration
---

## Zones de livraison personnalisées

Exemple, pour modifier la zone de Aubervilliers en zone 1, et rajouter La Courneuve en zone 2, dans « paramètres customisables » puis « zone de livraison », indiquer

```
[
  {
    "city": "Aubervilliers",
    "postal_code": "93300",
    "pricing_zone": "Zone_1"
  },
  {
    "city": "La Courneuve",
    "postal_code": "93120",
    "pricing_zone": "Zone_2"
  }
]
```
