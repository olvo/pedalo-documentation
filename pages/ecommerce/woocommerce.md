---
layout: page
title: WooCommerce
category: Documentation
permalink: ecommerce/woocommerce
subpage: true
toc: true
---

The Cyke official plugin is available in the [WordPress catalog](https://wordpress.org/plugins/cyke-logistics/).

### 💻 Installation

First of all, you need to be the owner of a WordPress site **with a WooCommerce store** set up.

1. On your WordPress Admin, go to the **Plugins** > **Add New** page.
2. Search `Cyke` and install the **Cyke** plugin.
3. Go to the **Plugins** > **Installed Plugins** page.
4. Activate the Cyke plugin.

The Cyke plugin is now installed!

### ⚙️ Configure your Cyke account

1. From the **Plugins** > **Installed Plugins** page, click on the **Settings** link within the Cyke list item.
2. Fill out the form with your Cyke Account information.
3. Save changes.

> You can get your API token on the general information page in your Cyke Account. From the sidebar: **Configuration > General information**, scroll down to the **API** section to reveal your token.

### 🚲 Create the Cyke shipping method

1. Go to the **WooCommerce** > **Settings** page and select the **Shipping** tab.
2. If you already have a local Shipping Zone configured, you can **Edit** it; otherwise, add a new one by clicking on **Add shipping zone**.
3. On the Shipping Zone form, click on **Add shipping method**.
4. Select **Bike Delivery with Cyke**, and add the shipping zone.
5. (optional) You can edit the name (displayed to your users) by clicking **Edit** under your shipping method name.
6. Save changes.

### 📦 Fill out dimensions of your shop products

The product dimensions are used to find the correct Cyke packaging for your order. If no dimensions are provided, the product will be ignored within the packaging.

_For each product:_

- Go to the product **Edit** page.
- Under the **Product Data** > **Shipping** section, fill out the **dimensions** and the **weight** of the product.
- Save changes.

### 👀 Find your Cyke deliveries

After a Cyke delivery is created by the plugin, you can find it on the admin page of the Order from which the delivery has been created.

- In case of success, the Cyke ID and a hyperlink are present under **Order details** > **Shipping** section.
- In case of an error, information about the error will be displayed in the **Order notes**, and you will need to manually create the delivery on Cyke.

### 🛠️ Troubleshooting

If customers cannot see timeslots in the checkout page

- Make sure you have correctly filled the credentials of your Cyke account in the Cyke settings section of the WooCommerce plugin. See [account configuration documentation](#%EF%B8%8F-configure-your-cyke-account).

If deliveries are not created on Cyke.

- Make sure you have specified dimensions for your products, otherwise the plugins won't include the packages, and deliveries won't be created.
