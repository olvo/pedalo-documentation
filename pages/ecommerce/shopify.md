---
layout: page
title: Shopify
category: Documentation
permalink: ecommerce/shopify
subpage: true
toc: true
---

First of all, you need to be the owner of a Shopify Store.

You must also **enable and configure Local Delivery** for your Shopify store. In fact, the order will create a Cyke delivery only if the customer chooses local delivery during checkout.

### 💻 Installation

You can install [the Cyke App](https://apps.shopify.com/cyke) from the Shopify App Store.

Onboarding and setup instructions are available in English and French directly within the app. If you prefer you can follow the rest of this guide.

### ⚙️ Configure your Cyke account

1. Go to the **Cyke Account** page using the left bar navigation menu.
2. Fill out the form with your Cyke Account information.
3. Test the connection.
4. If it is working, you can save changes.

> You can get your API token on the general information page in your Cyke Account. From the sidebar: **Configuration > General information**, scroll down to the **API** section to reveal your token.

### 📦 Create packagings for your shop products

For the app to create deliveries with the correct Cyke packages, you need to fill in information about each of your products that you want to be delivered using Cyke.

1. Go to the **Products** page using the left bar navigation menu. On this page, you will find the list of your shop products.
2. _For each product:_
   - Go to the product packaging page by clicking on the product line.
   - Fill out the form with either a **Cyke Package Type** (recommended) or the **dimensions** of the product.
   - Save changes.

### 🗓️ Add the date picker to your storefront

To allow users to select a delivery date, you can add a date picker block to your storefront.

> Note: If the user does not provide a date, or if the date picker is missing, the delivery will be scheduled for the next available day with a full-day timeslot.

1. Go to the store's **Themes** page, and click **Customize**.
2. From the top navigation bar, select the **Cart** page from the middle-right dropdown.
3. From the left sidebar, under **Template** > **Subtotal**, click **+ Add block** and select **Cyke Date Picker**.
4. (optional) You can reorder the Subtotal blocks to make the date picker more visible.
5. Save changes.

You should now be able to see the date picker **on the Cart page of your storefront**.

### 👀 Find your Cyke deliveries

Cyke deliveries created by the app will be listed on the app's home page.

- In case of failure, you can view the order and create the delivery manually on Cyke.
- In case of success, you can view the newly created delivery on Cyke using the link from the list.
