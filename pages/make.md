---
layout: page
title: Make Integration
category: Documentation
permalink: make
toc: true
---

## Introduction

[Make](https://www.make.com/){:target="_blank"} is an automation platform, which enables you to plug several web applications together.

There is no official Cyke integration on Make, but here is a tutorial written by Manon from Aïny, who is using Make to automate her delivery flow through Make.

It relies on the [Cyke API]({% link pages/api.md %}).

Similarly to the API integration, we strongly recommend to operate deliveries created manually before starting to work on an automated integration. Especially, you can probably export deliveries from an ecommerce website, then import the file on Cyke.


## How-to

### Authentication

To be able to use the plugin, you need two pieces of information:
- API token. You can find it in the general information page in your Cyke Account. From the sidebar: Configuration > General information, and scroll down to the API section to reveal your token.
- Email address. This must be the email address of your Cyke account.

### Setup

Use the 'HTTP' module:

<img src="assets/make-flow.png" alt="Zapier Zap" />
<p class="post-meta">Make integration example.</p>

In the first module, fetch the identifier for the type of package you need to select:

<img src="assets/make-packages.png" alt="Zapier Zap" />
<p class="post-meta">Get packages from the Cyke API.</p>

The second module is the delivery creation:

<img src="assets/make-deliveries.png" alt="Zapier Zap" />
<p class="post-meta">Create a delivery on Cyke through the API.</p>

<img src="assets/make-params.png" alt="Zapier Zap" />
<p class="post-meta">Fill the params depending on your needs.</p>

Using the params from the [Cyke API documentation]({% link pages/api.md %}#code-examples-2):

```json
{% raw %}
  {
    "dropoff": {
      "slot_starting_at": "{{if(1.`Livraison après-midi ?`; formatDate(1.`Livraison_début`; "YYYY-MM-DD") + "T14:00:00.000+02:00"""; formatDate(1.`Livraison_début`; "YYYY-MM-DD") + "T09:00:00.000+02:00""")}},
      "slot_ending_at": "{{if(1.`Livraison après-midi ?`; formatDate(1.Livraison_fin; "YYYY-MM-DD") + "T18:00:00.000+02:00"""; formatDate(1.Livraison_fin; "YYYY-MM-DD") + "T13:00:00.000+02:00""")}},
      "place": {
        "recipient_name": "{{1.Nom_du_client[]}}",
        "recipient_phone": "{{1.Telephone[]}}",
        "company_name": "{{1.Projet[]}}",
        "address": "{{1.Adresse[]}}",
        "postal_code": "{{1.`Code postal`[]}}",
        "city": "{{1.Ville[]}}",
        "address_instructions": "{{1.`Complément adresse`[]}}"
      }
    },
    "packages": [
        {
          "package_type_id": {{11.data[].id}},
          "amount": 1
        }
    ],
    "comments": "{{1.`Commentaire pour Olvo`}}",
    "client_order_reference": "{{1.`reference envoi`}}",
    "bring_back_containers": false
  }
{% endraw %}
```

Do not try to use 'values' or this kind of things, because the structure of the params expected by Cyke wouldn't work with this module. Just keep using 'map'.


## Helping hand

If you are not able to use Make and connect your ecommerce website to Cyke, we may do it for you. Get in touch with us. The deadline will depend on our availability and we will charge a fee.
