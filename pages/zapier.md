---
layout: page
title: Zapier Integration
category: Documentation
permalink: zapier
redirect_from:
  - /pedalo-documentation/zapier
toc: true
---

## Introduction

[Zapier](https://zapier.com/){:target="\_blank"} is an automation platform, which enables you to plug several web applications together.

There is a Cyke integration on Zapier, which enables you to create new deliveries on Cyke, without having to develop a [complete API integration]({% link pages/api.md %}).

Similarly to the API integration, we strongly recommend to operate deliveries created manually before starting to work on an automated integration. Especially, you can probably export deliveries from an ecommerce website, then import the file on Cyke.

## How-to

### 1. Get access

First, you need to have a client account on Cyke. This means a messenger company using Cyke has opened an account for you and you are able to create deliveries.

Then get in touch with us ([cyke@cargonautes.fr](mailto:cyke@cargonautes.fr)) so that we give you access to the Zapier Cyke integration.

### 2. Authentication

To be able to use the plugin, you must set three pieces of information:

- **API token**. You can find it in the general information page in your Cyke Account. From the sidebar: _Configuration_ > _General information_, and scroll down to the API section to reveal your token.
- **Email address**. This must be the email address of your Cyke account.
- The **Cyke instance URL**. It is the URL under which you are using Cyke. Most of the time it is `https://www.cyke.io`

### 3. Gather mandatory data

Make sure your website gathers the mandatory information to send a delivery to Cyke :

- **Delivery slot** (starting time and end time)
- **Recipient Address** (name, phone, address, city, postal code)
- **Packages Information** (amount, dimensions, weight)

Your ecommerce website may ask the user using integrated plugins, such as [https://woocommerce.com/fr-fr/products/delivery-slots-for-woocommerce/](https://woocommerce.com/fr-fr/products/delivery-slots-for-woocommerce/) for Woocommerce (but there are other plugins on every ecommerce platform)

### 4. Plug it

Use Zapier to connect your ecommerce website to Cyke.

<img src="assets/zapier-zap.png" alt="Zapier Zap" />
<p class="post-meta">Zapier integration example : intermediate steps are sometimes necessary to set up delivery slot and packages.</p>

<img src="assets/zapier-cyke-app.png" alt="Zapier Cyke integration" />
<p class="post-meta">Cyke integration configuration.</p>

### 5. Handle errors

In the real world, everything can go wrong some day. As an example, Cyke may reject a delivery creation because some data is missing. For this reason, we recommend to set up the [Zapier Manager](https://zapier.com/apps/zapier-manager/integrations) on your account so that you can receive a notification for any error happening on the Cyke integration.

<img src="assets/zapier-error.png" alt="Zapier Error" />
<p class="post-meta">Zapier error example : any error in a Zap is notified on a Slack channel.</p>

## Helping hand

If you are not able to use Zapier and connect your ecommerce website to Cyke, we may do it for you. Get in touch with us. The deadline will depend on our availability and we will charge a fee.
