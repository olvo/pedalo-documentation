---
layout: error
title: Cyke est en maintenance !
permalink: maintenance
---

<div class="error">
  <img class="error-image" src="assets/cyke.png" alt="Logo Cyke" />

  <div class="error-type">
    Cyke est en maintenance !
  </div>
  <p class="error-message">
    L'application revient dans quelques minutes.
  </p>
  <p>
    Si cette page reste visible et que vous êtes bloqués, vous pouvez nous demander plus d'infos sur <a href="mailto:cyke@cargonautes.fr">cyke@cargonautes.fr</a>.
  </p>
</div>
