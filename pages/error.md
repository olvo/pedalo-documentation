---
layout: error
title: Oups ! Notre application a déraillé
permalink: error
redirect_from:
  - /pedalo-documentation/error
---

<div class="error">
  <img class="error-image" src="assets/cyke.png" alt="Logo Cyke" />

  <div class="error-type">
    Oups ! Cyke vient de dérailler !
  </div>
  <div class="error-message">
    Une notification vient d'être transmise à l'équipe technique avec les détails de l'erreur.
  </div>
  <a class="btn-back" href="https://www.cyke.io">Revenir à l'accueil de Cyke</a>
</div>
