---
Documentation for Cyke
---

## Develoment

To work locally with this project, you'll have to follow the steps below:

1. Clone this project
1. Download dependencies: `bundle`
1. Build and preview: `bundle exec jekyll serve`
1. Add content

The above commands should be executed from the root directory of this project.

Read more at Jekyll's [documentation](https://jekyllrb.com/docs/).

## Deployment

Any push on the main branch will be deployed by Gitlab CI on [https://olvo.gitlab.io/pedalo-documentation/](https://olvo.gitlab.io/pedalo-documentation/)
